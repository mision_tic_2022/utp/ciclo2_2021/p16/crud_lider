package com.utp.p16.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.utp.p16.controlador.DatabaseController;
import com.utp.p16.modelo.vo.Lider;

public class DaoLider {
    
    public Lider buscar_lider(String documento_identidad) {
        Lider objLider = new Lider();
        try {
            Connection conn = DatabaseController.conectar_bd();
            // Preparar la consulta
            String query = "SELECT * FROM Lider WHERE Documento_Identidad = ?";
            PreparedStatement pStatement = conn.prepareStatement(query);
            // Enviar el parámetro a la consulta
            pStatement.setString(1, documento_identidad);
            // Ejecutar el query y almacenar la respuesta
            ResultSet respuesta = pStatement.executeQuery();
            // Validar que exista un registro como respuesta
            if (respuesta.next()) {
                objLider.setId(respuesta.getInt("ID_Lider"));
                objLider.setNombre(respuesta.getString("Nombre"));
                objLider.setPrimer_apellido(respuesta.getString("Primer_Apellido"));
                objLider.setSegundo_apellido( respuesta.getString("Segundo_Apellido") );
                objLider.setSalario(respuesta.getInt("Salario"));
                objLider.setCargo(respuesta.getString("Cargo"));
                objLider.setCiudad_residencia(respuesta.getString("Ciudad_Residencia"));
                objLider.setClasificacion(respuesta.getInt("Clasificacion"));
                objLider.setDocumento_identidad(respuesta.getString("Documento_Identidad"));
                objLider.setFecha_nacimiento(respuesta.getString("Fecha_Nacimiento"));
            }
            //cerrar la conexión a la BD
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        return objLider;
    }// Fin del método buscar_lider

    public void insertar_lider(Lider objLider) {
        // Query
        String query = "INSERT INTO Lider(ID_Lider, Nombre, Primer_Apellido, Segundo_Apellido, Salario, Ciudad_Residencia, Cargo, Clasificacion, Documento_Identidad, Fecha_Nacimiento) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            Connection conn = DatabaseController.conectar_bd();
            // Objeto para preparar la consulta antes de ejecutarla
            PreparedStatement pStatement = conn.prepareStatement(query);

            pStatement.setInt(1, objLider.getId());
            pStatement.setString(2, objLider.getNombre());
            pStatement.setString(3, objLider.getPrimer_apellido());
            pStatement.setString(4, objLider.getSegundo_apellido());
            pStatement.setInt(5, objLider.getSalario());
            pStatement.setString(6, objLider.getCiudad_residencia());
            pStatement.setString(7, objLider.getCargo());
            pStatement.setInt(8, objLider.getClasificacion());
            pStatement.setString(9, objLider.getDocumento_identidad());
            pStatement.setString(10, objLider.getFecha_nacimiento());

            // Ejecutar la consulta
            // pStatement.executeUpdate();

            if (pStatement.executeUpdate() == 1) {
                System.out.println("Lider creado con éxito!");
            }
            //cerrar la conexión a la BD
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }
    }


    public void actualizar_lider(Lider lider, int id){

        String query = "UPDATE Lider SET ID_Lider = ?, Nombre = ?, Primer_Apellido  = ?, Segundo_Apellido = ?, Salario  = ?, Ciudad_Residencia  = ?, Cargo = ?, Clasificacion = ?, Documento_Identidad = ?,Fecha_Nacimiento  = ? WHERE ID_Lider = ?";
        
        try {
            Connection conn = DatabaseController.conectar_bd();
            PreparedStatement pStatement = conn.prepareStatement(query);
            pStatement.setInt(1, lider.getId());
            pStatement.setString(2, lider.getNombre());
            pStatement.setString(3, lider.getPrimer_apellido());
            pStatement.setString(4, lider.getSegundo_apellido());
            pStatement.setInt(5, lider.getSalario());
            pStatement.setString(6, lider.getCiudad_residencia());
            pStatement.setString(7, lider.getCargo());
            pStatement.setInt(8, lider.getClasificacion());
            pStatement.setString(9, lider.getDocumento_identidad());
            pStatement.setString(10, lider.getFecha_nacimiento());
            pStatement.setInt(11, id);

            if( pStatement.executeUpdate() == 1 ){
                System.out.println("¡Información actualizada con éxito!");
            }
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }

    }
    
}
