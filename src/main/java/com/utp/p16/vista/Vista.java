package com.utp.p16.vista;

import java.util.Scanner;


import com.utp.p16.controlador.Controlador;
import com.utp.p16.modelo.vo.Lider;

public class Vista{
    //Atributos
    private Controlador controlador;

    public Vista(){
        this.controlador = new Controlador();
    }
   

    public void mostrar_lider(Lider lider){
        System.out.println("------------------LIDER-------------------");
        System.out.println("");
        System.out.println("Id: "+lider.getId());
        System.out.println("Nombre: "+lider.getNombre());
        System.out.println("Primer apellido: "+lider.getPrimer_apellido());
        System.out.println("Segundo apellido: "+lider.getSegundo_apellido());
        System.out.println("Salario: $"+lider.getSalario());
        System.out.println("Ciudad residencia: "+lider.getCiudad_residencia());
        System.out.println("Cargo: "+lider.getCargo());
        System.out.println("Clasificación: "+lider.getClasificacion());
        System.out.println("Documento de identidad: "+lider.getDocumento_identidad());
        System.out.println("Fecha nacimiento: "+lider.getFecha_nacimiento());
        System.out.println("");
        System.out.println("-------------------------------------------");
    }


    public Lider crear_formulario(){
        //Se crea el objeto lider
        Lider objLider = this.controlador.construir_lider();
        //Se manipula entrada por consola
        try (Scanner entrada = new Scanner(System.in)){
            //Solicitar el id
            System.out.println("Por favor ingrese el ID: ");
            objLider.setId( entrada.nextInt() );
            /*
            System.out.println("");
            String nombre = JOptionPane.showInputDialog(null, "Por favor ingrese el nombre ");
            objLider.setNombre(nombre);
            */
            System.out.println("Por favor ingrese el nombre del lider: ");
            objLider.setNombre( entrada.next() );
            

            System.out.println("Por favor ingrese el primer apellido: ");
            objLider.setPrimer_apellido( entrada.next() );

            System.out.println("Por favor ingrese el segundo apellido: ");
            objLider.setSegundo_apellido( entrada.next() );

            System.out.println("Por favor ingrese el salario: ");
            objLider.setSalario( entrada.nextInt() );

            System.out.println("Por favor ingrese la ciudad de residencia: ");
            objLider.setCiudad_residencia( entrada.next() );

            System.out.println("Por favor ingrese el cargo: ");
            objLider.setCargo( entrada.next() );

            System.out.println("Por favor ingrese la clasificación: ");
            objLider.setClasificacion( entrada.nextInt() );

            System.out.println("Por ingrese documento de identificación: ");
            objLider.setDocumento_identidad( entrada.next() );

            System.out.println("Por favor ingrese la fecha de nacimiento (yyyy-mm-dd)");
            objLider.setFecha_nacimiento( entrada.next() );
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
            System.out.println("Por favor intente mas tarde");
        }
        return objLider;
    }

    public void registrar_lider(){
        Lider objLider = this.crear_formulario();
        this.controlador.insertar_lider(objLider);
        System.out.println("-------INFORMACIÓN DEL LIDER CREADO----------");
        this.mostrar_lider(objLider);
        System.out.println("---------------------------------------------");
    }

    public void buscar_lider(){
        try (Scanner entrada = new Scanner(System.in)){
            
            System.out.println("Por favor ingrese el documento del lider que desea buscar:");
            Lider objLider = this.controlador.buscar_lider( entrada.next() );
            this.mostrar_lider(objLider);

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
    }


    public void actualizar_lider(){
        try (Scanner entrada = new Scanner(System.in)){
            
            System.out.println("Por favor ingrese el documento del lider que desea actualizar: ");
            Lider objLider = this.controlador.buscar_lider( entrada.next() );
            //Validar si el lider exite
            if(objLider.getNombre() == null){
                System.out.println("El lider con el documento ingresado no existe");
            }else{
                Lider lider = this.crear_formulario();
                this.controlador.actualizar_lider(lider, objLider.getId());
            }
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }
    }


    public void crear_menu(){

        System.out.println("------------CRUD LIDER-----------");
        System.out.println("1 -> Buscar lider");
        System.out.println("2 -> Actualizar lider");
        System.out.println("3 -> Registrar lider");
        System.out.println("0 -> Salir");
        System.out.println("Ingrese una opción:");

        try (Scanner entrada = new Scanner(System.in)){
            
            int opcion = entrada.nextInt();
            switch(opcion){
                case 1:
                    this.buscar_lider();
                    break;
                case 2:
                    this.actualizar_lider();
                    break;
                case 3:
                    this.registrar_lider();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    this.crear_menu();
                    break;
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }

    }

}