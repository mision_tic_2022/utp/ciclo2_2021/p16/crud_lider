package com.utp.p16.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.*;

import com.utp.p16.controlador.Controlador;
import com.utp.p16.modelo.vo.Lider;

public class VistaFormulario extends JFrame {
    /*************
     * Atributos
     ************/
    private JLabel lblNombre;
    private JLabel lblPrimerApellido;
    private JLabel lblSegundoApellido;
    private JLabel lblSalario;
    private JLabel lblDocumento;
    //Campos de texto
    private JTextField txtNombre;
    private JTextField txtPrimerApellido;
    private JTextField txtSegundoApellido;
    private JTextField txtSalario;
    private JTextField txtDocumento;
    //Botones
    private JButton btnBuscar;
    private JButton btnGuardar;
    //Constrolador
    private Controlador controlador;


     /***************
     * Constructor
     ***************/
    public VistaFormulario(){
        this.controlador = new Controlador();
        //Configurar la ventana
        this.setTitle("Formulario lider");
        this.setBounds(0, 0, 450, 200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        //Construccion de un contenedor con un gridlayout
        GridLayout gCenterLayout = new GridLayout(5, 2, 5, 5);
        Container centerContainer = new Container();
        centerContainer.setLayout(gCenterLayout);

        this.getContentPane().setLayout( new BorderLayout() );

        //Inicializar los elementos y agregarlos al contenedor

        /***********************
         * CONTENEDOR SUPERIOR
         ***********************/
        GridLayout gNorthLayout = new GridLayout(1, 3, 5, 5);
        Container northContainer = new Container();
        northContainer.setLayout(gNorthLayout);
        
        this.lblDocumento = new JLabel("Documento identidad: ");
        northContainer.add(this.lblDocumento);
        this.txtDocumento = new JTextField();
        northContainer.add(this.txtDocumento);
        this.btnBuscar = new JButton("Buscar");
        northContainer.add(this.btnBuscar);

        

        /***********************
         * CONTENEDOR CENTRAL
         ***********************/
        this.lblNombre = new JLabel("Nombre: ");
        centerContainer.add(lblNombre);
        this.txtNombre = new JTextField();
        centerContainer.add(this.txtNombre);

        this.lblPrimerApellido = new JLabel("Primer apellido: ");
        centerContainer.add(this.lblPrimerApellido);
        this.txtPrimerApellido = new JTextField();
        centerContainer.add(this.txtPrimerApellido);

        this.lblSegundoApellido = new JLabel("Segundo apellido: ");
        centerContainer.add(this.lblSegundoApellido);
        this.txtSegundoApellido = new JTextField();
        centerContainer.add(this.txtSegundoApellido);

        this.lblSalario = new JLabel("Salario: ");
        centerContainer.add(this.lblSalario);
        this.txtSalario = new JTextField();
        centerContainer.add(this.txtSalario);

        centerContainer.add( new JLabel() );
        this.btnGuardar = new JButton("Guardar");
        centerContainer.add(this.btnGuardar);

        //Manejador de eventos
        this.btnBuscar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                String documento = txtDocumento.getText();
                Lider objLider = controlador.buscar_lider(documento);
                //Setear campos de texto
                txtNombre.setText( objLider.getNombre() );
                txtPrimerApellido.setText( objLider.getPrimer_apellido() );
                txtSegundoApellido.setText( objLider.getSegundo_apellido() );
                txtSalario.setText( ""+objLider.getSalario() );
            }
        } );

        this.add(centerContainer, BorderLayout.CENTER);
        this.add(northContainer, BorderLayout.NORTH);
        this.add(new Button("WEST"), BorderLayout.WEST);
        this.add( new Button("EAST"), BorderLayout.EAST );
        this.add( new Button("SOUTH"), BorderLayout.SOUTH );
    }

    
}
