package com.utp.p16.controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseController {

    //Atributos
    private static final String DATABASE = "ProyectosConstruccion.db";


    // Método para conectar a la base de datos
    public static Connection conectar_bd()  throws SQLException{
        String url = "jdbc:sqlite:"+DATABASE;
            // Objeto de conexión
            return DriverManager.getConnection(url);
    }// Fin del método conectar_bd
    
}
