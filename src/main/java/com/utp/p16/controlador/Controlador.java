package com.utp.p16.controlador;

import com.utp.p16.modelo.dao.DaoLider;
import com.utp.p16.modelo.vo.Lider;

public class Controlador {
    /*************
     * Atributos
     *************/
    private DaoLider daoLider;

    /*************
     * Constructor
     *************/
    public Controlador() {
        daoLider = new DaoLider();
    }

    /*************
     * Acciones
     *************/


    // Consultas

    // Método para buscar un lider en la BD
    public Lider buscar_lider(String documento_identidad) {
        return  daoLider.buscar_lider(documento_identidad);
    }// Fin del método buscar_lider

    public void insertar_lider(Lider objLider) {
        daoLider.insertar_lider(objLider);
    }

    public Lider construir_lider() {
        return new Lider();
    }

    public Lider contruir_lider(int id, String nombre, String primer_apellido, String segundo_apellido, int salario,String ciudad_residencia, String cargo, int clasificacion, String documento_identidad, String fecha_nacimiento) {
        return new Lider(id, nombre, primer_apellido, segundo_apellido, salario, ciudad_residencia, cargo,
                clasificacion, documento_identidad, fecha_nacimiento);
    }


    public void actualizar_lider(Lider lider, int id){
        daoLider.actualizar_lider(lider, id);
    }

}